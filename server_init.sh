#!/usr/bin/env bash

apt update && apt install -y wget docker docker-compose build-essential python-dev libreadline-dev libbz2-dev libssl-dev libsqlite3-dev libxslt1-dev libxml2-dev git curl mysql-client libmysqlclient-dev python3-dev

# curl https://pyenv.run | bash
# echo '# pyenv ' >> ~/.bashrc
# echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.profile
# echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.profile
# echo 'eval "$(pyenv init --path)"' >> ~/.profile
# echo 'export PATH="$HOME/.pyenv/bin:$PATH"' >> ~/.profile
# echo 'eval "$(pyenv init -)"' >> ~/.profile
# echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.profile
# sed -e '1s/^/export PYENV_ROOT="$HOME/.pyenv"\n/' ~/.profile
# sed -e '1s/^/export PATH="$PYENV_ROOT/bin:$PATH"\n/' ~/.profile
# sed -e '1s/^/eval "$(pyenv init --path)"\n/' ~/.profile
# echo 'export PYENV_ROOT="$HOME/.pyenv"' | cat - .profile > temp && mv temp .profile
# echo 'export PATH="$PYENV_ROOT/bin:$PATH"' | cat - .profile > temp && mv temp .profile
# echo 'eval "$(pyenv init --path)"' | cat - .profile > temp && mv temp .profile
# echo 'eval "$ (pyenv init -)"' >> ~/.bashrc
# exec $SHELL
# echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
# env
# source ~/.bashrc
# pyenv install 3.9.2
# pip install -r requirements.txt
