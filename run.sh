#!/bin/bash

if [ "$(docker network ls | grep bot_network)" == "" ]; then
  docker network create bot_network
fi

# docker kill $(docker ps -q)
docker run --network bot_network --name=mysql -e MYSQL_ROOT_HOST='%' -e MYSQL_ROOT_USERNAME=root -e MYSQL_ROOT_PASSWORD=pass -v "$(pwd)/data:/var/lib/mysql" -p 3316:3306 --rm -d mysql:5.7 mysqld &

while ! mysqladmin ping -h "127.0.0.1" -P 3316 --silent; do
  echo 'ping'
  sleep 1
done

echo 'ready'
#sleep 9999

docker run \
    --rm \
    --name bot \
    --network bot_network \
    --volume "$PWD:/usr/src/myapp" \
    --workdir /usr/src/myapp \
    python:$(cat .python-version.txt) \
    bash -c "python -m venv bot_venv && source bot_venv/bin/activate && pip install -r requirements.txt \
            && python -u db_init.py && python -u telegram_bot.py"

docker stop mysql
docker stop bot

