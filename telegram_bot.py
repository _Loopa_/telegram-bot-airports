import telebot
import subprocess
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from search import search_by_code
from db_Airport_template import Airport
from vincenty import vincenty

Token = '1798291410:AAFXWdU60RMgnsXB-sJpOFhDthIl_tTzCtA'
bot = telebot.TeleBot(Token)

engine = create_engine('mysql://root:pass@mysql:3306/mydb', echo=True)
Session_search = sessionmaker(bind=engine)
s = Session_search()

code = ''
code_1 = {}
code_2 = {}


@bot.message_handler(content_types=['text'])
def start(message):
    if message.text == '/start':
        bot.send_message(message.from_user.id, "Hello. Input one code to receive info about airport, two - to receive "
                                               "distance between two airports")
        bot.register_next_step_handler(message, get_codes)
    else:
        bot.send_message(message.from_user.id, 'Call /start')


def get_codes(message):
    global code, code_1, code_2
    if len(message.text.split()) == 1:
        try:
            info = search_by_code(message.text, Airport, s)
            print(info['name'])
            bot.send_message(message.from_user.id, info['name'])
            bot.register_next_step_handler(message, get_codes)
        except TypeError:
            bot.send_message(message.from_user.id, "incorrect argument, input again")
            bot.register_next_step_handler(message, get_codes)

    elif len(message.text.split()) == 2:
        try:
            code_1 = search_by_code(message.text.split()[0], Airport, s)
            code_2 = search_by_code(message.text.split()[1], Airport, s)
            distance = vincenty([float(code_1['latitude']), float(code_1['longitude'])], [float(code_2['latitude']),
                                                                                          float(code_2['longitude'])])
            print("distance: ", distance)
            bot.send_message(message.from_user.id, 'distance: ' + str(distance))
            bot.send_message(message.from_user.id, "if you'd like to search distance again, call /start")
        except TypeError:
            bot.send_message(message.from_user.id, "incorrect arguments, input again")
            bot.register_next_step_handler(message, get_codes)
    else:
        bot.send_message(message.from_user.id, "too many arguments, input again")
        bot.register_next_step_handler(message, get_codes)


print("The Telegram-script is running")
bot.polling(none_stop=True)
